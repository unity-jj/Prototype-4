using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSmashAttack : MonoBehaviour
{
  private float cooldown = 1f;
  private float hangTime = .35f;
  private float smashSpeed = 35f;
  private bool smashing = false;
  private float explosionForce = 40f;
  private float explosionRadius = 15f;
  private float floorY;
  private Rigidbody playerRigidbody;

  // Start is called before the first frame update
  void Start()
  {
    playerRigidbody = GetComponent<Rigidbody>();
  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Space) && !smashing)
    {
      StartCoroutine(DoSmashAttack());
    }
  }

  IEnumerator DoSmashAttack()
  {
    smashing = true;
    floorY = transform.position.y;
    Enemy[] enemies = FindObjectsOfType<Enemy>();
    float jumpTime = Time.time + hangTime;

    while (Time.time < jumpTime)
    {
      playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, smashSpeed);
      yield return null;
    }

    while (transform.position.y > floorY)
    {
      playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, -smashSpeed * 2);
      yield return null;
    }

    for (int i = 0; i < enemies.Length; i++)
    {
      if (enemies[i] == null) continue;

      enemies[i].GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, explosionRadius, 0.0f, ForceMode.Impulse);
    }

    smashing = false;
  }
}
