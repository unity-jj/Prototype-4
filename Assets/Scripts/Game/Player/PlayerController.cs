using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public float speed = 5f;
  private Rigidbody playerRb;
  private GameObject focalPoint;
  private PlayerPowerup playerPowerup;

  void Start()
  {
    playerRb = GetComponent<Rigidbody>();
    focalPoint = GameObject.Find("Focal Point");
    playerPowerup = GetComponent<PlayerPowerup>();
  }

  void Update()
  {
    float forwardInput = Input.GetAxis("Vertical");
    playerRb.AddForce(focalPoint.transform.forward * speed * forwardInput);
  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.CompareTag("Enemy") && playerPowerup.currentPowerup == PowerupType.Pushback)
    {
      Rigidbody enemyRb = collision.gameObject.GetComponent<Rigidbody>();
      Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);

      enemyRb.AddForce(awayFromPlayer * playerPowerup.powerupStrength, ForceMode.Impulse);
    }
  }
}
