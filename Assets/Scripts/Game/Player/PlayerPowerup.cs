using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPowerup : MonoBehaviour
{
  [SerializeField]
  private GameObject powerupIndicator;

  [SerializeField]
  private GameObject missilePrefab;

  public PowerupType currentPowerup = PowerupType.None;
  public float powerupStrength = 15f;
  private GameObject temporalMissile;
  private Coroutine powerupCountdown;
  private PlayerSmashAttack playerSmashAttack;

  void Start()
  {
    playerSmashAttack = GetComponent<PlayerSmashAttack>();
  }

  void Update()
  {
    powerupIndicator.transform.position = transform.position + new Vector3(0, -0.5f, 0);

    if (powerupCountdown != null)
    {
      StopCoroutine(powerupCountdown);
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.CompareTag("Powerup"))
    {
      powerupIndicator.gameObject.SetActive(true);
      Destroy(other.gameObject);
      currentPowerup = other.gameObject.GetComponent<Powerup>().powerupType;

      StartCoroutine(PowerupCountdownRoutine());

      if (currentPowerup == PowerupType.Missiles)
      {
        LaunchMissiles();
      }
      else if (currentPowerup == PowerupType.Smash)
      {
        playerSmashAttack.enabled = true;
      }
    }
  }

  IEnumerator PowerupCountdownRoutine()
  {
    yield return new WaitForSeconds(7);

    if (currentPowerup == PowerupType.Smash)
    {
      playerSmashAttack.enabled = false;
    }

    currentPowerup = PowerupType.None;
    powerupIndicator.gameObject.SetActive(false);
  }

  void LaunchMissiles()
  {
    foreach (var enemy in FindObjectsOfType<Enemy>())
    {
      temporalMissile = Instantiate(
          missilePrefab,
          transform.position + Vector3.up,
          Quaternion.identity
      );
      temporalMissile.GetComponent<MissileBehavior>().Fire(enemy.transform);
    }
  }
}
