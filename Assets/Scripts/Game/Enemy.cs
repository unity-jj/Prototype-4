using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
  public float speed;
  private Rigidbody enemyRb;
  private GameObject player;

  // Start is called before the first frame update
  void Start()
  {
    enemyRb = GetComponent<Rigidbody>();
    player = GameObject.Find("Player");
  }

  // Update is called once per frame
  void Update()
  {
    DestroyIfOutOfBounds();
    FollowPlayer();
  }

  void FollowPlayer()
  {
    Vector3 playerDirection = player.transform.position - transform.position;

    enemyRb.AddForce(playerDirection.normalized * speed);
  }

  void DestroyIfOutOfBounds()
  {
    if (transform.position.y < -10)
    {
      Destroy(gameObject);
    }
  }
}
