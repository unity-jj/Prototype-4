using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
  public GameObject[] powerupPrefabs;
  public GameObject[] enemyPrefabs = new GameObject[2];
  private int enemyCount;
  private int waveNumber = 1;
  private float spawnRange = 9f;

  // Start is called before the first frame update
  void Start()
  {
    GeneratePowerup();
    SpawnEnemyWave(waveNumber);
  }

  // Update is called once per frame
  void Update()
  {
    CountEnemies();
    MoveToNextWaveIfThereAreNoEnemiesLeft();
  }

  void GeneratePowerup()
  {
    int randomPowerupIndex = Random.Range(0, powerupPrefabs.Length);
    GameObject randomPowerup = powerupPrefabs[randomPowerupIndex];

    Instantiate(randomPowerup, GenerateSpawnPosition(), randomPowerup.transform.rotation);
  }

  void CountEnemies()
  {
    enemyCount = FindObjectsOfType<Enemy>().Length;
  }

  void MoveToNextWaveIfThereAreNoEnemiesLeft()
  {
    if (enemyCount == 0)
    {
      waveNumber += 1;
      GeneratePowerup();
      SpawnEnemyWave(waveNumber);
    }
  }

  void SpawnEnemyWave(int enemiesToSpawn)
  {
    for (int i = 0; i < enemiesToSpawn; i++)
    {
      GameObject randomEnemy = GetRandomEnemy();
      Instantiate(randomEnemy, GenerateSpawnPosition(), randomEnemy.transform.rotation);
    }
  }

  GameObject GetRandomEnemy()
  {
    int randomIndex = Random.Range(0, enemyPrefabs.Length);

    return enemyPrefabs[randomIndex];
  }

  Vector3 GenerateSpawnPosition()
  {
    float spawnPosX = Random.Range(-spawnRange, spawnRange);
    float spawnPosZ = Random.Range(-spawnRange, spawnRange);

    return new Vector3(spawnPosX, 0, spawnPosZ);
  }
}
